sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("si.gov.fu.ovd.LP.controller.LP_main", {
		oTabela: null,
		tajliMobilci: [{
			id: "mobilci0",
			ime: "Plačilni nalog",
			ikona: "sap-icon://simple-payment",
			link: "https://hedhpdtst.fu.sigov.si:51064",
			rola: "DURS-S_POR_FPLUS_PN039A"
		}, {
			id: "mobilci1",
			ime: "Odredba o vračilu dokumentov",
			ikona: "sap-icon://request",
			link: "https://hedhpdtst.fu.sigov.si:51066",
			rola: "DURS-S_POR_FPLUS_OVD038A"
		}, {
			id: "mobilci2",
			ime: "Nedelovanje povezanih sistemov",
			ikona: "sap-icon://cancel-maintenance",
			link: "https://hedhpdtst.fu.sigov.si:51067",
			rola: "DURS-S_POR_FPLUS_ALERTS036A"
		}, {
			id: "mobilci3",
			ime: "Izpis podatkov o izdanih dokumentih",
			ikona: "sap-icon://table-view",
			link: "https://hedhpdtst.fu.sigov.si:51065",
			rola: "DURS-S_POR_FPLUS_BPPRINT037A"
		}],
		tajliIzvrsba: [{
			id: "izvrsba0",
			ime: "Izvršba",
			ikona: "sap-icon://batch-payments",
			link: "https://hedhpdtst.fu.sigov.si:51063",
			rola: "DURS-S_POR_FPLUS_IZTERJAVA040A"
		}],
		tajliPredlog: [{
			id: "predlog0",
			ime: "Predlog za izvedbo postopka o prekršku",
			ikona: "sap-icon://create",
			link: "https://hedhpdtst.fu.sigov.si:51093",
			rola: "DURS-S_POR_FPLUS_PP041A"
		}, {
			id: "predlog1",
			ime: "Predlog za obravnavo kaznivega dejanja",
			ikona: "sap-icon://create-form",
			link: "https://hedhpdtst.fu.sigov.si:51072",
			rola: "DURS-S_POR_FPLUS_KD042A"
		},
		{
			id: "predlog2",
			ime: "Pregled obravnavanih predlogov",
			ikona: "sap-icon://inspection",
			link: "https://hedhpdtst.fu.sigov.si:51122",
			rola: "DURS-S_POR_FPLUS_OP043A"
		}
	],
	tajliNadzorBlagajn: [{
			id: "nadzorBlagajn0",
			ime: "Pošiljanje obvestil Mobilnim oddelkom",
			ikona: "sap-icon://activity-items",
			link: "https://hedhpdtst.fu.sigov.si:51144",
			rola: "DURS-S_POR_FPLUS_IZTERJAVA040A"
		}, {
			id: "nadzorBlagajn1",
			ime: "Izpis zgodovine posredovanih alarmov",
			ikona: "sap-icon://inbox",
			link: "https://hedhpdtst.fu.sigov.si:51145",
			rola: "DURS-S_POR_FPLUS_IZTERJAVA040A"
		}
	],
		messageStrip: [],
		onInit: function() {
			jQuery.sap.require("sap.m.MessageBox");
			
			this.getView().byId("mobilci").setVisible(false);
			this.getView().byId("TileContainerExpandedMobilci").setVisible(false);
			this.getView().byId("izvrsba").setVisible(false);
			this.getView().byId("TileContainerExpandedIzvrsba").setVisible(false);
			this.getView().byId("predlogi").setVisible(false);
			this.getView().byId("TileContainerExpandedPP").setVisible(false);
			this.getView().byId("nadzorBlagajn").setVisible(false);
			this.getView().byId("TileContainerExpandedNadzor").setVisible(false);
			this.getView().byId("versionText").setText("verzija: " + LPVersion);
			var that = this;
			LP.puo = getCookie();

			//LOGIN
			//odpremo nek dialog, tako da je zadaj disabled in da se ga ne da zapreti oz. enablat zadaj
			var busyDialog = new sap.m.BusyDialog({
				title: "Prijava",
				text: "...prijava je v teku."
			});
			this.loginDialog = new sap.m.Dialog({
				title: "Prijava v aplikacijo",
				type: "Standard",
				afterOpen: function(oEvent) {
					this.getContent()[1].focus();
				},
				content: [new sap.m.Input({
						id: "username",
						placeholder: "Uporabniško ime",
						submit: function() {
							that.loginDialog.getEndButton().firePress();
						}
					}),
					new sap.m.Input({
						id: "password",
						type: "Password",
						placeholder: "Geslo",
						submit: function() {
							that.loginDialog.getEndButton().firePress();
						}
					})
				],
				beforeOpen: function () {
					this.getContent()[1].setValue("");
				},
				beginButton: new sap.m.Button({
					text: 'Zamenjava gesla',
					press: function(oEvent) {
						busyDialog.open();
						LP.puo = logIn(oEvent.getSource().getParent().getContent()[0].getValue().toUpperCase(), oEvent.getSource().getParent().getContent()[
							1].getValue());
						switch (LP.puo.code) {
							case 0:
								that.loginDialog.close();
								setTimeout(function() {
									that.onLogoffPressed(0, that);
								}, LP.puo.validity.getTime() * 60 * 1000);
								var changePassDialog = new sap.m.Dialog({
									title: "Zamenjava gesla",
									type: "Standard",
									content: [new sap.m.Input({
											id: "pass1",
											type: "Password",
											placeholder: "Novo geslo",
											submit: function() {
												that.loginDialog.getEndButton().firePress();
											}
										}),
										new sap.m.Input({
											id: "pass2",
											type: "Password",
											placeholder: "Ponovi novo geslo",
											submit: function() {
												that.loginDialog.getEndButton().firePress();
											}
										})
									],
									endButton: new sap.m.Button({
										text: "Zamenjaj geslo",
										press: function(oEvent) {
											var pass1 = oEvent.getSource().getParent().getContent()[0].getValue();
											var pass2 = oEvent.getSource().getParent().getContent()[1].getValue();
											if (pass1 === pass2) {
												if (changePassword(pass1, LP.puo.jwt).PWD_CHANGE_SSO === 1) {
													changePassDialog.destroy();
													that.onLogoffPressed(0, that);
												} else {
													sap.m.MessageToast.show("Zamenjava gesla ni bila uspešna!", {
														duration: 2000,
														animationDuration: 1000,
														my: "center center",
														at: "center center"
													});
												}
											} else {
												sap.m.MessageToast.show("Gesli se ne ujemata!", {
													duration: 2000,
													animationDuration: 1000,
													my: "center center",
													at: "center center"
												});
											}
										}
									})
								});
								changePassDialog.open();
								break;
							case 2:
							default:
								sap.m.MessageToast.show("Uporabniško ime ali geslo nista pravilna!", {
									duration: 2000,
									animationDuration: 1000,
									my: "center center",
									at: "center center"
								});
								gthat.getView().byId("userText").setText("");
								break;
							case 3:
								sap.m.MessageToast.show("Uporabniško ime je zaklenjeno!", {
									duration: 2000,
									animationDuration: 1000,
									my: "center center",
									at: "center center"
								});
								gthat.getView().byId("userText").setText("");
								break;

						}
						busyDialog.close();
					},
					error: function() {
						sap.m.MessageToast.show("Prijava ni uspela, poskusite ponovno!", {
							duration: 2000,
							animationDuration: 1000,
							my: "center center",
							at: "center center"
						});
						busyDialog.close();
					}
				}),
				endButton: new sap.m.Button({
					text: 'Prijava',
					press: function(oEvent) {
						//avtenticiramo na CRM
						busyDialog.open();
						LP.puo = logIn(oEvent.getSource().getParent().getContent()[0].getValue().toUpperCase(), oEvent.getSource().getParent().getContent()[
							1].getValue());
						switch (LP.puo.code) {
							case 0:
								that.loginDialog.close();
								setTimeout(function() {
									that.onLogoffPressed(0, that);
								}, (LP.puo.validity.getTime() - new Date().getTime()));
								that.getView().byId("userText").setText(LP.puo.IP_UNAME);
								
								var oModelMobilci = new sap.ui.model.json.JSONModel();
								oModelMobilci.setData(that.tajliMobilci.filter(function(r) {
									return LP.puo.ET_ROLES.indexOf(r.rola) !== -1;
								}));
								that.getView().setModel(oModelMobilci, "tajliMobilci");
								
								var oModelIzvrsba = new sap.ui.model.json.JSONModel();
								oModelIzvrsba.setData(that.tajliIzvrsba.filter(function(r) {
									return LP.puo.ET_ROLES.indexOf(r.rola) !== -1;
								}));
								that.getView().setModel(oModelIzvrsba, "tajliIzvrsba");
								
								var oModelPP = new sap.ui.model.json.JSONModel();
								oModelPP.setData(that.tajliPredlog.filter(function(r) {
									return LP.puo.ET_ROLES.indexOf(r.rola) !== -1;
								}));
								that.getView().setModel(oModelPP, "tajliPredlog");
								
								var oModelNadzor = new sap.ui.model.json.JSONModel();
								oModelNadzor.setData(that.tajliNadzorBlagajn.filter(function(r) {
									return LP.puo.ET_ROLES.indexOf(r.rola) !== -1;
								}));
								that.getView().setModel(oModelNadzor, "tajliNadzorBlagajn");
								
								if(that.getView().byId("TileContainerExpandedMobilci").getContent().length == 0){
									that.getView().byId("mobilci").setVisible(false);
									that.getView().byId("TileContainerExpandedMobilci").setVisible(false);
								}
								else{
									that.getView().byId("mobilci").setVisible(true);
									that.getView().byId("TileContainerExpandedMobilci").setVisible(true);
								}
								
								if(that.getView().byId("TileContainerExpandedIzvrsba").getContent().length == 0){
									that.getView().byId("izvrsba").setVisible(false);
									that.getView().byId("TileContainerExpandedIzvrsba").setVisible(false);
								}
								else{
									that.getView().byId("izvrsba").setVisible(true);
									that.getView().byId("TileContainerExpandedIzvrsba").setVisible(true);
								}
								
								if(that.getView().byId("TileContainerExpandedPP").getContent().length == 0){
									that.getView().byId("predlogi").setVisible(false);
									that.getView().byId("TileContainerExpandedPP").setVisible(false);
								}
								else{
									that.getView().byId("predlogi").setVisible(true);
									that.getView().byId("TileContainerExpandedPP").setVisible(true);
								}
								
								if(that.getView().byId("TileContainerExpandedNadzor").getContent().length == 0){
									that.getView().byId("nadzorBlagajn").setVisible(false);
									that.getView().byId("TileContainerExpandedNadzor").setVisible(false);
								}
								else{
									that.getView().byId("nadzorBlagajn").setVisible(true);
									that.getView().byId("TileContainerExpandedNadzor").setVisible(true);
								}
								
								sap.m.MessageToast.show("PRIJAVLJENI STE V TEST!", {
									duration: 5000,
									animationDuration: 1000,
									my: "center center",
									at: "center center"
								});
								break;
							case 2:
							default:
								sap.m.MessageToast.show("Uporabniško ime ali geslo nista pravilna!", {
									duration: 2000,
									animationDuration: 1000,
									my: "center center",
									at: "center center"
								});
								that.getView().byId("userText").setText("");
								break;
							case 3:
								sap.m.MessageToast.show("Uporabniško ime je zaklenjeno!", {
									duration: 2000,
									animationDuration: 1000,
									my: "center center",
									at: "center center"
								});
								that.getView().byId("userText").setText("");
								break;
							case -1:
								sap.m.MessageToast.show("Prijava ni uspela, poskusite ponovno!", {
									duration: 2000,
									animationDuration: 1000,
									my: "center center",
									at: "center center"
								});
								busyDialog.close();
								that.getView().byId("userText").setText("");
								break;

						}
						busyDialog.close();

					}
				}),
				escapeHandler: function() {
					sap.m.MessageToast.show("Potrebno je vnesti uporabniško ime in geslo!", {
						duration: 2000,
						animationDuration: 1000,
						my: "center center",
						at: "center center"
					});
					that.getView().byId("userText").setText("");
				}
			});
			if (LP.puo === "") {
				this.loginDialog.open();
				if (window.document.documentMode) {
					sap.m.MessageBox.error("Aplikacija Fplus ni podprta za Internet Explorer, prosim uporabite MS Edge.", {
				    title: "Napaka",                                 // default
				    actions: null
					});
				}
			} else {
				if (window.document.documentMode) {
					sap.m.MessageBox.error("Aplikacija Fplus ni podprta za Internet Explorer, prosim uporabite MS Edge.", {
				    title: "Napaka",                                 // default
				    actions: null
					});
				}
				setTimeout(function() {
					that.onLogoffPressed(0, that);
				}, ((new Date(LP.puo.validity)).getTime() - new Date().getTime()));
				that.getView().byId("userText").setText(LP.puo.IP_UNAME);
				
				var oModelMobilci = new sap.ui.model.json.JSONModel();
				oModelMobilci.setData(that.tajliMobilci.filter(function(r) {
					return LP.puo.ET_ROLES.indexOf(r.rola) !== -1;
				}));
				that.getView().setModel(oModelMobilci, "tajliMobilci");
				
				var oModelIzvrsba = new sap.ui.model.json.JSONModel();
				oModelIzvrsba.setData(that.tajliIzvrsba.filter(function(r) {
					return LP.puo.ET_ROLES.indexOf(r.rola) !== -1;
				}));
				that.getView().setModel(oModelIzvrsba, "tajliIzvrsba");
				
				var oModelPP = new sap.ui.model.json.JSONModel();
				oModelPP.setData(that.tajliPredlog.filter(function(r) {
					return LP.puo.ET_ROLES.indexOf(r.rola) !== -1;
				}));
				that.getView().setModel(oModelPP, "tajliPredlog");
				
				var oModelNadzor = new sap.ui.model.json.JSONModel();
				oModelNadzor.setData(that.tajliNadzorBlagajn.filter(function(r) {
					return LP.puo.ET_ROLES.indexOf(r.rola) !== -1;
				}));
				that.getView().setModel(oModelNadzor, "tajliNadzorBlagajn");
				
				if(that.getView().byId("TileContainerExpandedMobilci").getContent().length == 0){
					that.getView().byId("mobilci").setVisible(false);
					that.getView().byId("TileContainerExpandedMobilci").setVisible(false);
				}
				else{
					that.getView().byId("mobilci").setVisible(true);
					that.getView().byId("TileContainerExpandedMobilci").setVisible(true);
				}
				
				if(that.getView().byId("TileContainerExpandedIzvrsba").getContent().length == 0){
					that.getView().byId("izvrsba").setVisible(false);
					that.getView().byId("TileContainerExpandedIzvrsba").setVisible(false);
				}
				else{
					that.getView().byId("izvrsba").setVisible(true);
					that.getView().byId("TileContainerExpandedIzvrsba").setVisible(true);
				}
				
				if(that.getView().byId("TileContainerExpandedPP").getContent().length == 0){
					that.getView().byId("predlogi").setVisible(false);
					that.getView().byId("TileContainerExpandedPP").setVisible(false);
				}
				else{
					that.getView().byId("predlogi").setVisible(true);
					that.getView().byId("TileContainerExpandedPP").setVisible(true);
				}
				
				if(that.getView().byId("TileContainerExpandedNadzor").getContent().length == 0){
					that.getView().byId("nadzorBlagajn").setVisible(false);
					that.getView().byId("TileContainerExpandedNadzor").setVisible(false);
				}
				else{
					that.getView().byId("nadzorBlagajn").setVisible(true);
					that.getView().byId("TileContainerExpandedNadzor").setVisible(true);
				}
			}

			console.log(this.messageStrip);
			console.log(this);

			var that = this;
			jQuery.ajax({
				method: "GET",
				url: ajPi + "/semaforCRM",
				async: false,
				success: function(s_data) {
					that.messageStrip.push(s_data);
				},
				error: function() {
					that.messageStrip.push({
						TEXT: "CRM NE DELUJE",
						STATUS: "Error"
					});
				}
			});
			jQuery.ajax({
				method: "GET",
				url: ajPi + "/semaforERP",
				async: false,
				success: function(s_data) {
					that.messageStrip.push(s_data);
				},
				error: function() {
					that.messageStrip.push({
						TEXT: "ERP NE DELUJE",
						STATUS: "Error"
					});
				}
			});
			jQuery.ajax({
				method: "GET",
				url: ajPi + "/semaforEDS",
				async: false,
				success: function(s_data) {
					that.messageStrip.push(s_data);
				},
				error: function() {
					that.messageStrip.push({
						TEXT: "EDS NE DELUJE",
						STATUS: "Error"
					});
				}
			});
			
		},

		onTilePress: function(oEvt) {
			if(oEvt.getSource().getBindingContext("tajliMobilci") != undefined){
				var context = oEvt.getSource().getBindingContext("tajliMobilci").getModel().getData();
			}
			else if(oEvt.getSource().getBindingContext("tajliIzvrsba") != undefined){
				var context = oEvt.getSource().getBindingContext("tajliIzvrsba").getModel().getData();
			}
			else if(oEvt.getSource().getBindingContext("tajliPredlog") != undefined){
				var context = oEvt.getSource().getBindingContext("tajliPredlog").getModel().getData();
			}
			else if(oEvt.getSource().getBindingContext("tajliNadzorBlagajn") != undefined){
				var context = oEvt.getSource().getBindingContext("tajliNadzorBlagajn").getModel().getData();
			}
			//console.log(this.tajli[parseInt(context.getPath().replace(/^\D+/g, ""))].link);
			var busyDialog = new sap.m.BusyDialog({
				title: "Nalaganje",
				text: "...nalagam podatke."
			});
			for(var i = 0; i < context.length; i++){
				if(oEvt.getSource().getHeader() == context[i].ime){
					window.open(context[i].link, "_self");
				}
			}
			
		},
		/*onTilePress: function(oEvt) {
			window.open(oEvt.getSource().getUrl(), "_self");
		},*/
		onLogoffPressed: function(oEvent, that) {
			logOut();
			if (oEvent === 0) {
				that.loginDialog.open();
				that.getView().byId("userText").setText("");
				that.getView().byId("mobilci").setVisible(false);
				that.getView().byId("TileContainerExpandedMobilci").setVisible(false);
				that.getView().byId("izvrsba").setVisible(false);
				that.getView().byId("TileContainerExpandedIzvrsba").setVisible(false);
				that.getView().byId("predlogi").setVisible(false);
				that.getView().byId("TileContainerExpandedPP").setVisible(false);
				that.getView().byId("nadzorBlagajn").setVisible(false);
				that.getView().byId("TileContainerExpandedNadzor").setVisible(false);
			} else {
				this.loginDialog.open();
				this.getView().byId("userText").setText("");
				this.getView().byId("mobilci").setVisible(false);
				this.getView().byId("TileContainerExpandedMobilci").setVisible(false);
				this.getView().byId("izvrsba").setVisible(false);
				this.getView().byId("TileContainerExpandedIzvrsba").setVisible(false);
				this.getView().byId("predlogi").setVisible(false);
				this.getView().byId("TileContainerExpandedPP").setVisible(false);
				this.getView().byId("nadzorBlagajn").setVisible(false);
				this.getView().byId("TileContainerExpandedNadzor").setVisible(false);
			}
		}
	});
});