function logIn(user, pass) {
	var auth = doLogIn(user, pass);
	if (auth.code === 0) {
		setCookie(auth);
	}
	return auth;
}

function logOut() {
	deleteCookie();
}

function setCookie(auth) {
	document.cookie = 'PORauth=' + JSON.stringify(auth) + '; expires=' + (auth.validity).toGMTString() + '; path=/';
}

function getCookie() {
	var cookie = document.cookie.replace(/(?:(?:^|.*;s* )PORauth*=s*([^;]*).*$)|^.*$/, '$1');
	return cookie !== "" ? JSON.parse(cookie) : cookie;
}

function deleteCookie() {
	document.cookie = "PORauth=; max-age=-1; path=/;";
}

function setOfflineCookie(data){
	var date = data[3];
	date = JSON.parse(CryptoJS.AES.decrypt(atob(date), "12345").toString(CryptoJS.enc.Utf8));
	document.cookie = 'Izterjava-login=' + JSON.stringify(data) + '; expires=' + new Date(date).toUTCString() + '; path=/';
}

function doLogIn(user, pass) {
	var auth = {};
	auth.code = -1;
	var loginData = {};
	loginData.user = btoa(CryptoJS.AES.encrypt(user, "porMO").toString());
	loginData.pass = btoa(CryptoJS.AES.encrypt(pass, "porMO").toString());
	jQuery.ajax({
		method: "POST",
		url: crmConn + "/login",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(loginData),
		async: false,
		success: function(s_data) {
			console.log(s_data);
			if (s_data.code === 0) {
				auth = s_data;
				auth.validity = new Date((new Date()).getTime() + 30 * 60 * 1000);
				auth.ET_ROLES = auth.ET_ROLES.map(function(x){return x[""];});
				
				jQuery.sap.require("sap.ui.core.format.DateFormat");
				var dateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "MM/dd/yyyy"});
				var date = dateFormat.format(new Date((new Date()).getTime() + 72 * 60 * 60 * 1000));
					
				var content = [
					btoa(CryptoJS.AES.encrypt(JSON.stringify(auth), "12345").toString()), 
					CryptoJS.MD5(user).toString(), 
					CryptoJS.MD5(pass).toString(),
					btoa(CryptoJS.AES.encrypt(JSON.stringify(date), "12345").toString())
				];
					
				setOfflineCookie(content);
			}
		},
		error: function(err) {
			auth.code = -1;
			auth.err = err;
		}
	});
	return auth;
}

function changePassword(pass, jwt) {
	var auth = {};
	auth.code = -1;
	var newPass = btoa(CryptoJS.AES.encrypt(pass, "porMO").toString())
	jQuery.ajax({
		method: "POST",
		url: crmConn + "/change_pwd",
		dataType: "json",
		async: false,
		contentType: "application/json",
		data: JSON.stringify({
			"new_pwd": newPass
		}),
		headers: {
			"authorization":jwt
		},
		success: function(s_data) {
			console.log(s_data);
			if (s_data.PWD_CHANGE_SSO === 1) {
				auth.code = 1;
				auth = s_data;
			}

		},
		error: function(err) {
			auth.err = err
		}
	});
	return auth;
}